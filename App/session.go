package main

import (
	"crypto/rand"
	b64 "encoding/base64"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Role int

const (
	ROLE_NONE Role = iota
	ROLE_ADMIN
	ROLE_AUTHOR
)

var (
	key []byte
)

func InitSessions() {
	log.Info("Initializing session system...")

	key = make([]byte, 128)
	_, err := rand.Read(key)
	if err != nil {
		log.Fatal("Unable to initialize sessions:", err)
	}

	log.Debug("JWT signing key:", b64.StdEncoding.EncodeToString(key))

	log.Info("Session system initialized.")
}

func VerifySession(w http.ResponseWriter, r *http.Request) Role {
	sessionCookie, err := r.Cookie("Session")
	if err == nil {
		// Session cookie found, verify
		ss := sessionCookie.Value
		token, err := jwt.Parse(ss, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return key, nil
		})

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			role, _ := strconv.Atoi(claims["sub"].(string))
			return Role(role)
		}

		log.Notice("Invalid JWT", ss, ":", err)
	}

	// Session not found or invalid
	setNewSession(w, ROLE_NONE)
	return ROLE_NONE
}

func SetRole(w http.ResponseWriter, r *http.Request, role Role) {
	setNewSession(w, role)
}

func setNewSession(w http.ResponseWriter, role Role) {
	claims := &jwt.StandardClaims{
		Issuer:    pInfo.Domain,
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Add(43200 * time.Second).Unix(),
		Subject:   strconv.Itoa(int(role)),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	ss, err := token.SignedString(key)
	if err != nil {
		log.Error("Error during session creation:", err)
		return
	}

	sessionCookie := &http.Cookie{
		Name:     "Session",
		Path:     "/",
		Value:    ss,
		Expires:  time.Now().Add(43200 * time.Second),
		MaxAge:   43200,
		SameSite: http.SameSiteStrictMode,
	}

	http.SetCookie(w, sessionCookie)
}
