package main

import (
	"os"

	"github.com/op/go-logging"
)

var log = logging.MustGetLogger("zortacsite")

var format = logging.MustStringFormatter(
	`%{color}%{time:2006-01-02T15:04:05.999-0700} %{shortfunc} ▶ %{level} %{id:03x} %{message}%{color:reset}`,
)

func InitLogging() {
	stdlogger := logging.NewLogBackend(os.Stderr, "", 0)
	stdloggerFormatter := logging.NewBackendFormatter(stdlogger, format)

	if !pInfo.Debug {
		stdloggerLeveled := logging.AddModuleLevel(stdloggerFormatter)
		stdloggerLeveled.SetLevel(logging.INFO, "")
		logging.SetBackend(stdloggerLeveled)
	} else {
		logging.SetBackend(stdloggerFormatter)
	}
}
