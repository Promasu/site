package main

import (
	"github.com/globalsign/mgo"
)

var DB *mgo.Database

func InitDB() {
	log.Info("Initializing Database...")
	session, err := mgo.Dial("mongodb://" + pInfo.DbUser + ":" + pInfo.DbPass + "@" + pInfo.DbHost)
	if err != nil {
		log.Fatal("Could not establish database connection:", err)
	}
	DB = session.DB(pInfo.DbName)
	log.Info("Database connection established.")
}
