package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/gorilla/mux"
	"github.com/segmentio/ksuid"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/gomail.v2"
)

type User struct {
	Username        string `bson:"username"`
	Hash            []byte `bson:"password"`
	PermissionLevel Role   `bson:"role"`
}

type CaptchaApiResponse struct {
	Success   bool   `json:"success"`
	Timestamp string `json:"timestamp"`
	Hostname  string `json:"hostname"`
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	/*
		Admin user has to be added manually to database to provide initial login. Create a document in the "users"
		collection following the template below:

		{
			"username": <username here>,
			"password": <bcrypt hashed password here>,
			"role": 1
		}
	*/
	if r.Method != http.MethodPost {
		http.Error(w, "Logging in must happen through a POST request.", http.StatusMethodNotAllowed)
		return
	}

	username := r.PostFormValue("username")
	password := r.PostFormValue("password")

	// Get User from DB
	query := &bson.M{
		"username": username,
	}

	var result User

	err := DB.C("users").Find(query).One(&result)
	if err != nil {
		http.Error(w, "Username or password invalid.", http.StatusUnauthorized)
		return
	}

	if bcrypt.CompareHashAndPassword(result.Hash, []byte(password)); err == nil {
		SetRole(w, r, result.PermissionLevel)
	} else {
		http.Error(w, "Username or password invalid.", http.StatusUnauthorized)
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func UseraddHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)

	if role != ROLE_ADMIN {
		http.Error(w, "Tried to add new user without required permissions.", http.StatusUnauthorized)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Adding a user must happen through a POST request.", http.StatusMethodNotAllowed)
		return
	}

	username := r.PostFormValue("username")
	password := r.PostFormValue("password")
	perms := r.PostFormValue("permissionlevel")
	var verifiedPerms Role

	switch perms {
	case "ADMIN":
		verifiedPerms = ROLE_ADMIN
	case "AUTHOR":
		verifiedPerms = ROLE_AUTHOR
	default:
		verifiedPerms = ROLE_NONE
	}

	if username == "" || password == "" {
		http.Error(w, "Username and password must not be empty.", http.StatusBadRequest)
		return
	}

	// Check if the User already exists
	query := &bson.M{
		"username": username,
	}

	if n, _ := DB.C("users").Find(query).Limit(1).Count(); n > 0 {
		http.Error(w, "User already exists.", http.StatusConflict)
		return
	}

	// All checks passed, create the user
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		log.Error("Password hash failed with error:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	userrec := &bson.M{
		"username": username,
		"password": hash,
		"role":     int(verifiedPerms),
	}

	err = DB.C("users").Insert(userrec)
	if err != nil {
		log.Error("Could not insert user record into database:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)

}

func UserdelHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)

	if role != ROLE_ADMIN {
		http.Error(w, "Tried to delete user without required permissions.", http.StatusUnauthorized)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Deleting a user must happen through a POST request.", http.StatusMethodNotAllowed)
		return
	}

	username := r.PostFormValue("username")

	if username == "" {
		http.Error(w, "Can't delete an empty user.", http.StatusBadRequest)
		return
	}

	query := &bson.M{
		"username": username,
	}

	err := DB.C("users").Remove(query)
	if err == mgo.ErrNotFound {
		http.Error(w, "The requested user was not found in the database.", http.StatusBadRequest)
		return
	} else if err != nil {
		log.Error("Could not delete user from database:", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	return
}

func MessageHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Sending a message must happen through a POST request.", http.StatusMethodNotAllowed)
	}

	captchaResponse := r.PostFormValue("g-recaptcha-response")

	captchaValid := pInfo.Recaptcha.VerifyResponse(captchaResponse)

	if !captchaValid {
		http.Error(w, "Invalid captcha.", http.StatusUnauthorized)
		log.Warning("Error during captcha verification:", pInfo.Recaptcha.LastError())
		return
	}

	name := r.PostFormValue("name")
	email := r.PostFormValue("email")
	message := r.PostFormValue("message")

	var emailCheck = regexp.MustCompile(`^.+@.+\..+$`)

	redirectURI := strings.Split(r.Referer(), "?")[0]

	// Message validation
	if name == "" || message == "" {
		http.Redirect(w, r, redirectURI+"?messageSuccess=0", http.StatusSeeOther)
		return
	} else if !emailCheck.MatchString(email) {
		http.Redirect(w, r, redirectURI+"?messageSuccess=0", http.StatusSeeOther)
		return
	} else if len(message) > 2000 {
		http.Redirect(w, r, redirectURI+"?messageSuccess=0", http.StatusSeeOther)
		return
	}

	log.Info("New Message:", name, email, message)

	dialer := gomail.NewDialer(pInfo.MailServer, pInfo.MailPort, pInfo.MailUser, pInfo.MailPass)

	confirmation := gomail.NewMessage()

	confirmation.SetAddressHeader("From", "site@promasu.tech", "Adrian Nöthlich")
	confirmation.SetAddressHeader("To", email, name)
	confirmation.SetHeader("Subject", "Your Message to Promasu")

	confirmation.SetBody("text/plain", fmt.Sprintf(`Hello %s!
I've received your message submitted via the contact form on my website.

If you have anything to add, please reply to this email.

If you did not use the contact form on my message, you can ignore this mail.

You can find a copy of your message below:

%s`, name, message))

	messageMail := gomail.NewMessage()

	messageMail.SetAddressHeader("From", email, name)
	messageMail.SetHeader("To", "i@promasu.tech")
	messageMail.SetHeader("Subject", "Message from promasu.tech")
	messageMail.SetBody("text/plain", message)

	if err := dialer.DialAndSend(messageMail); err != nil {
		http.Error(w, "Error during message processing", http.StatusInternalServerError)
		log.Error("Error sending message:", err)
		return
	}

	if err := dialer.DialAndSend(confirmation); err != nil {
		http.Error(w, "Error during message processing", http.StatusInternalServerError)
		log.Error("Error sending message:", err)
		return
	}

	http.Redirect(w, r, redirectURI+"?messageSuccess=1", http.StatusSeeOther)
}

func PostCreateOrUpdateHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)

	if role != ROLE_ADMIN && role != ROLE_AUTHOR {
		http.Error(w, "Tried to update post without required permissions.", http.StatusUnauthorized)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Adding a post must happen through a POST request.", http.StatusMethodNotAllowed)
		return
	}

	r.ParseMultipartForm(32 << 20)

	postId := r.PostFormValue("id")
	title := r.PostFormValue("title")
	content := r.PostFormValue("content")
	author := r.PostFormValue("author")
	abstract := r.PostFormValue("abstract")
	hidden, _ := strconv.ParseBool(r.PostFormValue("hidden-check")) // Error can be ignored, hidden being false is the intended default behavior
	image, imageHeader, err := r.FormFile("image")

	// Sanitize linebreaks in content and abstract
	content = strings.Replace(content, "\r\n", "\n", -1)
	abstract = strings.Replace(abstract, "\r\n", "\n", -1)

	var imagepath string
	if err != nil && err != http.ErrMissingFile {
		// Error during upload
		http.Error(w, "Error during file upload.", http.StatusInternalServerError)
		log.Warning("Error during file upload:", err)
		return
	} else if err != http.ErrMissingFile {
		// Upload successful

		// Generate random id for file
		fileId := ksuid.New().String()
		err = os.Mkdir("static/resources/dl/"+fileId, os.ModePerm)
		if err != nil {
			http.Error(w, "Error during file upload.", http.StatusInternalServerError)
			log.Warning("Error during file upload:", err)
			return
		}

		file, err := os.OpenFile("static/resources/dl/"+fileId+"/"+imageHeader.Filename, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			http.Error(w, "Error during file upload.", http.StatusInternalServerError)
			log.Warning("Error during file upload:", err)
			return
		}
		defer file.Close()
		io.Copy(file, image)
		imagepath = "/resources/dl/" + fileId + "/" + imageHeader.Filename
	}

	// If no file was uploaded, this is fine, too

	if title == "" || content == "" {
		http.Error(w, "Title and content must not be empty.", http.StatusBadRequest)
		return
	}

	timestamp := time.Now().Unix()

	// All checks passed, create post
	post := bson.M{
		"title":     title,
		"author":    author,
		"hidden":    hidden,
		"timestamp": timestamp,
		"content":   content,
		"abstract":  abstract,
		"imageloc":  imagepath,
	}

	result := bson.M{}

	if bson.IsObjectIdHex(postId) {
		err = DB.C("posts").FindId(bson.ObjectIdHex(postId)).One(&result)
	} else {
		// An invalid Object ID is equivalent to the object not being found
		err = mgo.ErrNotFound
	}

	if err == mgo.ErrNotFound {
		// Post doesn't exist, create new one
		err = DB.C("posts").Insert(&post)
		if err != nil {
			log.Error("Could not insert post into database:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	} else if err != nil {
		log.Error("Could not search database for existing post.", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Keep original timestamp when updating
	if i, _ := result["timestamp"].(int64); i != 0 {
		post["timestamp"] = i
	}

	// If no new image was provided, keep old image
	if ip, _ := result["imageloc"].(string); ip != "" && imagepath == "" {
		post["imageloc"] = ip
	}

	err = DB.C("posts").UpdateId(bson.ObjectIdHex(postId), &post)
	if err != nil {
		log.Error("Could not insert post into database:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/listposts", http.StatusSeeOther)
}

func PostDeleteHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)

	if role != ROLE_ADMIN && role != ROLE_AUTHOR {
		http.Error(w, "Tried to delete post without required permissions.", http.StatusUnauthorized)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Deleting a post must happen through a POST request.", http.StatusMethodNotAllowed)
		return
	}

	postId := r.PostFormValue("id")

	if !bson.IsObjectIdHex(postId) {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	DB.C("posts").RemoveId(bson.ObjectIdHex(postId))

	http.Redirect(w, r, "/listposts", http.StatusSeeOther)
}

func PostVToggleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	id := vars["postid"]

	role := VerifySession(w, r)

	if role != ROLE_ADMIN && role != ROLE_AUTHOR {
		http.Error(w, "Tried to change post visibility without required permissions.", http.StatusUnauthorized)
		return
	}

	if bson.IsObjectIdHex(id) {
		var post Post
		err := DB.C("posts").FindId(bson.ObjectIdHex(id)).One(&post)

		if err != nil {
			log.Warning("Could not retreive requested post from database:", err)
			http.Error(w, "The requested post "+id+" was not found on this server.", http.StatusBadRequest)
			return
		}

		post.Hidden = !post.Hidden

		err = DB.C("posts").UpdateId(bson.ObjectIdHex(id), &post)
		if err != nil {
			log.Error("Could not insert post into database:", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/listposts", http.StatusSeeOther)

	} else {
		http.Error(w, "The id "+id+" is not a valid id.", http.StatusBadRequest)
		return
	}
}
